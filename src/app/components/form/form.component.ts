import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Flight } from './flight';
import {MatNativeDateModule, NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { PageService } from 'src/app/share/page.service';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  form !: FormGroup;
  flight !: Flight;
  flights:Array<Flight>=[];
  nativeDateAdapter!:NativeDateAdapter;

  constructor(private fb: FormBuilder,private pageService:PageService) {
    this.flight = new Flight("","","","",new Date(),new Date(),0,0,0);

   }

  ngOnInit(): void {
    this.form = this.fb.group({
      Fullname:['',Validators.required],
      From:['',Validators.required],
      To:['',Validators.required],
      Type:['',Validators.required],
      Departure:['',Validators.required],
      Arrival:['',Validators.required],
      Adults:['',[Validators.required,Validators.max(10)]],
      Children:['',[Validators.max(3)]],
      Infants:['',[Validators.max(2)]],
    })
    this.getListPages();
  }

  getListPages(){
    this.flights = this.pageService.getLists();
  }

  Submit(f:FormGroup):void{
    // this.flight.fullName = f.get('FullName')?.value;
    // this.flight.from = f.get('From')?.value;
    // this.flight.to = f.get('To')?.value;
    // this.flight.type = f.get('Type')?.value;
    // this.flight.arrival = f.get('Arrival')?.value;
    // this.flight.departure = f.get('Departure')?.value;
    // this.flight.adults = f.get('Adults')?.value;
    // this.flight.children = f.get('Children')?.value;
    // this.flight.infants = f.get('Infants')?.value;

    let form_record = new Flight(f.get('Fullname')?.value,
                                  f.get('From')?.value,
                                  f.get('To')?.value,
                                  f.get('Type')?.value,
                                  f.get('Arrival')?.value,
                                  f.get('Departure')?.value,
                                  f.get('Adults')?.value,
                                  f.get('Children')?.value,
                                  f.get('Infants')?.value);
      this.pageService.addList(form_record);
    }

}
