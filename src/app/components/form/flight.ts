export class Flight {
  constructor(
    public fullName: string,
    public from: string,
    public to: string,
    public type: string,
    public arrival: Date,
    public departure: Date,
    public adults: number,
    public children: number,
    public infants: number,
    ) {
      this.fullName = fullName;
      this.from = from;
      this.to = to;
      this.type = type;
      this.arrival = arrival;
      this.departure = departure;
      this.adults = adults;
      this.children = children;
      this.infants = infants;
    }
}
