import { Flight } from "../components/form/flight";
export class Mockflight {
  public static flight_list:Flight[]=[
    {
      fullName : "MobObOb",
      from : "BaiBua",
      to : "Bok",
      type : "One way",
      departure : new Date('2022-03-21') ,
      arrival : new Date('2022-03-21'),
      adults : 1,
      children : 0,
      infants : 0,
    },
    {
      fullName : "PayaNak",
      from : "Water",
      to : "Table",
      type : "Return",
      departure : new Date('2022-01-19') ,
      arrival : new Date('2022-02-1'),
      adults : 3,
      children : 2,
      infants : 1,
    },
    {
      fullName : "MobYai",
      from : "Door",
      to : "Bed",
      type : "One Way",
      departure : new Date('2022-03-10') ,
      arrival : new Date(),
      adults : 3,
      children : 2,
      infants : 1,
    },
  ];
}
