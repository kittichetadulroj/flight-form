import { Injectable } from '@angular/core';
import { Mockflight } from './mockflight';
import { Flight } from '../components/form/flight';
@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights:Flight[]=[]
  constructor() {
    this.flights = Mockflight.flight_list;
  }

  getLists():Flight[]{
    return this.flights;
  }

  addList(f:Flight):void{
    this.flights.push(f);
  }

}
